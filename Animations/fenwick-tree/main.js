let buildClicked = true;
let stepByStepClicked = false;
let nextStepClicked = false;
let execStarted = false;

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function pause(ms = 1000) {
  if (buildClicked) return;
  if (stepByStepClicked) {
    return sleep(ms);
  } else {
    while (true) {
      await sleep(10);
      if (nextStepClicked) {
        nextStepClicked = false;
        return;
      }
    }
  }
}

class Node {
  id = 0;
  from = 0;
  to = 0;
  acum = 0;
}

function labelForNode(node) {
  let numbersLength = 5;
  return `${node.id}: ${(node.id).toString(2).padStart(numbersLength, 0)}\n` +
      `S[${node.from}, ${node.to}] = ${node.acum}\n`;
}

async function build() {
  execStarted = true;
  let nodes = [];
  let edges = [];

  var data = {
    nodes: new vis.DataSet(nodes),
    edges: new vis.DataSet(edges)
  };

  var options = {
    layout: {
      hierarchical: {
        direction: "DU",
        sortMethod: "directed"
      }
    },
    nodes: {
      shape: "circle",
      borderWidth: 3,
      color: {
        border: "#6AA84F",
        background: "#3C78D8"
      },
      font: {
        color: "white",
        size: 15,
        strokeWidth: 1,
        strokeColor: "white"
      }
    },
    edges: {
      width: 8,
      background: {
        enabled: true,
        color: "#6AA84F"
      }
    },
    physics: {
      enabled: false
    }
  };

  let container = document.getElementById("SegmentTreeCanvas");

  network = new vis.Network(container, data, options);

  bit = document.getElementById("data").value.split(" ").map(Number);
  bit.unshift("0");
  let nodesList = [];

  for (let i = 0; i < bit.length; i++) {
    await pause(500);
    data.nodes.update([{id: i, label: `${bit[i]}`}]);
    nodesList.push({id: i, from: 0, to: 0, acum: bit[i]});
    network.fit();
  }

  if (stepByStepClicked)
    await pause(2000);

  for (let i = 0; i < bit.length; i++) {
    await pause(500);
    let from  = 0, to = i;
    if (i) from = i - (i & -i) + 1;
    nodesList[i].from = from;
    nodesList[i].to = to
    data.nodes.update([{id: i, label: labelForNode(nodesList[i])}]);
    if (i) {
      let dad = i - (i & -i);
      data.edges.update([{from: i, to: dad}]);
    }
    network.fit();
  }

  options.physics.enabled = true;
  network.setOptions(options);

  if (stepByStepClicked)
    await pause(1000);

  await pause(3000);

  for (let from = 1; from < bit.length; from++) {
    let to = from + (from & -from);
    if (to < bit.length) { 
      bit[to] += bit[from];
      nodesList[to].acum = bit[to];
      data.nodes.update([
        {id: to, label: labelForNode(nodesList[to]), color: {background: 'red'}}
      ]);
      await pause(500);
      data.nodes.update([{
        id: from, color: {background: "blue"}
      }]);
      await pause(2000);
      data.nodes.update([{id: to, color: {background: "#3C78D8"}}])
      data.nodes.update([{id: from, color: {background: "#3C78D8"}}]);
    }
  }
  if (stepByStepClicked)
    alert("Done");
  stepByStepClicked = false;
  buildClicked = false;
  nextStepClicked = false;
  execStarted = false;
}

let input = document.getElementById("data")
input.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        build();
    }
});

function nextStepOnClick() {
  nextStepClicked = true;
  buildClicked = false;
  stepByStepClicked = false;
  if (!execStarted) {
    build();
  }
}

async function justBuild() {
  // terminate next execution of step by step or next step if any
  // and then just end building
  buildClicked = true;
  if (execStarted) {
    stepByStepClicked = false;
    nextStepClicked = true;
    await sleep(20);
  } else {
    build();
  }
}

async function stepByStep() {
  // terminate next execution of step by step or next step if any
  // and then just continue with the next step
  stepByStepClicked = true;
  if (execStarted) {
    buildClicked = false;
    nextStepClicked = true;
    await sleep(20);
  } else {
    build();
  }
}

build();