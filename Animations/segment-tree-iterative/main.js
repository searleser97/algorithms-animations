let buildClicked = true;
let stepByStepClicked = false;
let nextStepClicked = false;
let execStarted = false;

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function pause(ms = 1000) {
  if (buildClicked) return;
  if (stepByStepClicked) {
    return sleep(ms);
  } else {
    while (true) {
      await sleep(10);
      if (nextStepClicked) {
        nextStepClicked = false;
        return;
      }
    }
  }
}

class Node {
  id = 0;
  pos = 0;
  value = 0;
}

function labelForNode(node) {
  let numbersLength = 5;
  return `val = ${node.value}\n` +
      `pos: ${node.pos}\n`;
}

let st = [];
let network = {};
let data = {};
let N = 0;
let nodesList = [];
const F = (a, b) => a + b;
let H = 0;


async function build() {
  execStarted = true;
  let nodes = [];
  let edges = [];

  data = {
    nodes: new vis.DataSet(nodes),
    edges: new vis.DataSet(edges)
  };

  var options = {
    layout: {
      hierarchical: {
        direction: "DU",
        sortMethod: "directed"
      }
    },
    nodes: {
      shape: "circle",
      borderWidth: 3,
      color: {
        border: "#6AA84F",
        background: "#3C78D8"
      },
      font: {
        color: "white",
        size: 15,
        strokeWidth: 1,
        strokeColor: "white"
      }
    },
    edges: {
      width: 8,
      background: {
        enabled: true,
        color: "#6AA84F"
      }
    }
  };

  let container = document.getElementById("SegmentTreeCanvas");

  network = new vis.Network(container, data, options);

  st = document.getElementById("data").value.split(" ").map(Number);
  N = st.length;
  for (let i = 0; i < N; i++) {
    st.unshift(0);
  }
  
  nodesList = [];

  H = Math.floor(Math.log2(N)) + 1;

  for (let i = 0; i < st.length; i++) {
    nodesList.push({id: i, pos: i, value: st[i]});
    if (i == 0) continue;
    if (i < N)
      data.nodes.update([{id: i, label: labelForNode(nodesList[i])}]);
    else
      data.nodes.update([{id: i, label: labelForNode(nodesList[i]), color: { background: "green"}}]);
    network.fit();
  }

  // await pause(1000);

  for (let i = N - 1; i; i--) {
    data.edges.update([{from: (i << 1), to: i}]);
    data.edges.update([{from: (i << 1 | 1), to: i}]);
    network.fit();
    // await pause(500);
  }

  await pause(10000);

  for (let i = N - 1; i; i--) {

    st[i] = F(st[i << 1], st[i << 1 | 1]);

    // ****** Animation *******
    nodesList[i].value = st[i];
    data.nodes.update([{id: i << 1, color: {border: "orange"}}]);
    data.nodes.update([{id: i << 1 | 1, color: {border: "orange"}}]);
    await pause(500);
    data.nodes.update([{id: i, label: labelForNode(nodesList[i]), color: {background: "orange"}}]);
    await pause(500);
    data.nodes.update([{id: i, color: { background: "#3C78D8" }}]);
    data.nodes.update([{id: i << 1, color: { border: "#6AA84F" }}]);
    data.nodes.update([{id: i << 1 | 1, color: { border: "#6AA84F" }}]);
    // ******* END ANIMATION ****
  }

  stepByStepClicked = false;
  buildClicked = false;
  nextStepClicked = false;
  execStarted = false;
}

async function update() {
  execStarted = true;
  let input = document.getElementById("dataU").value.split(" ").map(Number);
  
  let i = input[0], val = input[1];
  
  nodesList[i + N].value = val;
  data.nodes.update([{id: i + N, label: labelForNode(nodesList[i + N])}]);
  await pause(500);

  for (st[i += N] = val; i > 1; i >>= 1) {

    st[i >> 1] = F(st[i], st[i ^ 1]);

    // ****** Animation *******
    nodesList[i >> 1].value = st[i >> 1];
    data.nodes.update([{id: i, color: {border: "black"}}]);
    data.nodes.update([{id: i ^ 1, color: {border: "orange"}}]);
    await pause(500);
    data.nodes.update([{id: i >> 1, label: labelForNode(nodesList[i >> 1]), color: {background: "orange"}}]);
    await pause(500);
    data.nodes.update([{id: i >> 1, color: { background: "#3C78D8" }}]);
    data.nodes.update([{id: i, color: { border: "#6AA84F" }}]);
    data.nodes.update([{id: i ^ 1, color: { border: "#6AA84F" }}]);
    // ******* END ANIMATION ****
  }

  stepByStepClicked = false;
  buildClicked = false;
  nextStepClicked = false;
  execStarted = false;
}

async function query() {
  execStarted = true;
  let input = document.getElementById("dataQ").value.split(" ").map(Number);
  let output = document.getElementById("ansQ");

  let l = input[0], r = input[1];

  let ans = 0;

  for (l += N, r += N; l <= r; l >>= 1, r >>= 1) {
    data.nodes.update([{id: l, color: {background: "black"}}]);
    data.nodes.update([{id: r, color: {background: "orange"}}]);
    await pause(500);

    if (l & 1) {
      data.nodes.update([{id: l, color: {background: "#ff007b"}}]);
      ans = F(ans, st[l++]);
      output.value = ans;
    }
    if (~r & 1) {
      data.nodes.update([{id: r, color: {background: "red"}}]);
      ans = F(ans, st[r--]);
      output.value = ans;
    }

    await pause(500);
  }

  await pause(500);

  for (let i = 1; i < st.length; i++) {
    data.nodes.update([{id: i, color: { background: "#3C78D8" }}]);
  }

  stepByStepClicked = false;
  buildClicked = false;
  nextStepClicked = false;
  execStarted = false;
}

async function pushUtil(p, isLeft) {
  for (let s = H, k = 1 << (H - 1); s; s--, k >>= 1) {
    let i = p >> s;
    await pause(500);
    if (i) {
      if (isLeft)
        data.nodes.update([{id: i, color: {background: "purple"}}]);
      else
        data.nodes.update([{id: i, color: {background: "pink"}}]);
      await pause(500);
      data.nodes.update([{id: i, color: {background: "#3C78D8"}}]);
    }
    // if (u[i]) {
    //   apply(i << 1, d[i], k);
    //   apply(i << 1 | 1, d[i], k);
    //   u[i] = 0, d[i] = 0;
    // }
  }
}

async function push() {
  execStarted = true;
  let input = document.getElementById("dataP").value.split(" ").map(Number);
  let l = input[0] + N, r = input[1] + N;
  await pushUtil(l, true);
  await pause(1000);
  await pushUtil(r, false);
  stepByStepClicked = false;
  buildClicked = false;
  nextStepClicked = false;
  execStarted = false;
}

async function stepByStepPush() {
  stepByStepClicked = true;
  if (execStarted) {
    buildClicked = false;
    nextStepClicked = true;
    await sleep(20);
  } else {
    push();
  }
}


function nextStepQueryClick() {
  nextStepClicked = true;
  buildClicked = false;
  stepByStepClicked = false;
  if (!execStarted) {
    query();
  }
}

async function stepByStepQuery() {
  // terminate next execution of step by step or next step if any
  // and then just continue with the next step
  stepByStepClicked = true;
  if (execStarted) {
    buildClicked = false;
    nextStepClicked = true;
    await sleep(20);
  } else {
    query();
  }
}

async function justQuery() {
  // terminate next execution of step by step or next step if any
  // and then just end building
  buildClicked = true;
  if (execStarted) {
    stepByStepClicked = false;
    nextStepClicked = true;
    await sleep(20);
  } else {
    query();
  }
}

function nextStepUpdateClick() {
  nextStepClicked = true;
  buildClicked = false;
  stepByStepClicked = false;
  if (!execStarted) {
    update();
  }
}

async function stepByStepUpdate() {
  // terminate next execution of step by step or next step if any
  // and then just continue with the next step
  stepByStepClicked = true;
  if (execStarted) {
    buildClicked = false;
    nextStepClicked = true;
    await sleep(20);
  } else {
    update();
  }
}

async function justUpdate() {
  // terminate next execution of step by step or next step if any
  // and then just end building
  buildClicked = true;
  if (execStarted) {
    stepByStepClicked = false;
    nextStepClicked = true;
    await sleep(20);
  } else {
    update();
  }
}

function nextStepOnClick() {
  nextStepClicked = true;
  buildClicked = false;
  stepByStepClicked = false;
  if (!execStarted) {
    build();
  }
}

async function justBuild() {
  // terminate next execution of step by step or next step if any
  // and then just end building
  buildClicked = true;
  if (execStarted) {
    stepByStepClicked = false;
    nextStepClicked = true;
    await sleep(20);
  } else {
    build();
  }
}

async function stepByStep() {
  // terminate next execution of step by step or next step if any
  // and then just continue with the next step
  stepByStepClicked = true;
  if (execStarted) {
    buildClicked = false;
    nextStepClicked = true;
    await sleep(20);
  } else {
    build();
  }
}

let input = document.getElementById("data")
input.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        build();
    }
});


build();